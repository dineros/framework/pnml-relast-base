package de.tudresden.inf.st.pnml.base.constants;

public final class PnmlConstants {

    // general transitions
    public static final String TRANSITION_TYPE_DISCRETE = "discreteTransitionType";

    // topic elements
    public static final String TRANSITION_TYPE_TOPIC = "topicTransitionType";

    public static final String TRANSITION_TOPIC_PUBLISHERS_DEF_KEY = "publishers";
    public static final String TRANSITION_TOPIC_PUBLISHER_DEF_KEY = "publisher";
    public static final String TRANSITION_TOPIC_PORT_ID_DEF_KEY = "id";

    public static final String TRANSITION_TOPIC_SUBSCRIBERS_DEF_KEY = "subscribers";
    public static final String TRANSITION_TOPIC_SUBSCRIBER_DEF_KEY = "subscriber";

    // general properties
    public static final String NODE_KEY = "node";
    public static final String CHANNEL = "channel";
    public static final String TYPE_KEY = "type";
    public static final String TOOL_SPEC_KEY = "de.tudresden.inf.st.pnml.distributedPN";

    // service related keys
    public static final String TRANSITION_TYPE_SERVICE = "serviceTransitionType";
    public static final String SERVICE_NAME = "serviceName";
    public static final String TRANSITION_SERVICE_SERVER_IN_KEY = "serverInput";
    public static final String TRANSITION_SERVICE_SERVER_OUT_KEY = "serverOutput";
    public static final String TRANSITION_SERVICE_CHANNELS_KEY = "channels";
    public static final String TRANSITION_SERVICE_CHANNEL_KEY = "channel";
    public static final String TRANSITION_SERVICE_CHANNEL_ID_KEY = "cid";
    public static final String TRANSITION_SERVICE_REQUEST_KEY = "request";
    public static final String TRANSITION_SERVICE_RESPONSE_KEY = "response";

    // pub-sub related keys
    public static final String INPUT_LIMIT_KEY = "inputlimit";
    public static final String OUTPUT_LIMIT_KEY = "outputlimit";
    public static final String SUBNET_KEY = "subnet";
    public static final String TOPIC_KEY = "topic";
    public static final String TOPIC_NAME = "topicName";

    // signal keys
    public static final String TRANSITION_ID_KEY = "transitionID";
    public static final String INPUT_SIGNAL_ID_KEY = "inputsignalID";
    public static final String PLACE_ID_KEY = "placeID";
    public static final String CURRENT_VALUE_KEY = "initialvalue";
    public static final String CLAUSE_KEY = "inputsignalclause";
    public static final String INPUT_SIGNALS_DEF = "inputsignals";
    public static final String INPUT_SIGNAL_DEF = "inputsignal";
    public static final String INPUT_SIGNAL_ID_DEF = "inputsignalID";
    public static final String INPUT_SIGNAL_INIT_VALUE_DEF = "initialvalue";
    public static final String SIGNAL_VALUE_PAGE_ID = "signalValuePage";

    public static final String TOPIC_PUBLISHER = "topicPub";
    public static final String TOPIC_SUBSCRIBER = "topicSub";
    public static final String SERVICE_CLIENT = "serviceClient";
    public static final String SERVICE_SERVER = "serviceServer";
    public static final String TEMPLATE_NET = "DinerosTemplate";

    // arc types
    public static final String ARC_TYPE_KEY = "type";
    public static final String DEFAULT_ARC = "default";
    public static final String INHIBITOR_ARC = "inhibitor";

    // LRPN keys
    public static final String CHANNEL_PORTS_KEY = "ports";
    public static final String CHANNEL_PORT_KEY = "port";
    public static final String CHANNEL_PLACE_KEY = "placeId";
    public static final String CHANNEL_NAME_KEY = "name";
    public static final String CHANNEL_PLACE_TYPE_KEY = "placeType";
    public static final String CHANNEL_LIMIT_KEY = "limit";
    public static final String CHANNEL_SERVER_CAPACITY_KEY = "serverCapacity";
    public static final String CHANNEL_PLACE_TYPE_SUB_KEY = "sub";
    public static final String CHANNEL_PLACE_TYPE_PUB_KEY = "pub";
    public static final String CHANNEL_PLACE_TYPE_CLIENT_RES_KEY = "cres";
    public static final String CHANNEL_PLACE_TYPE_CLIENT_REQ_KEY = "creq";
    public static final String CHANNEL_PLACE_TYPE_SERVER_RES_KEY = "sres";
    public static final String CHANNEL_PLACE_TYPE_SERVER_REQ_KEY = "sreq";
    public static final String PORTS_CRES_KEY = "cResponsePlace";
    public static final String PORTS_CREQ_KEY = "cRequestPlace";

    // page properties
    public static final String PAGE_TYPE_SERVER = "serverPrototype";
    public static final String PAGE_TYPE_NODE = "nodePage";
    public static final String PAGE_SERVER_INSTANCE_SUFFIX = "INSTANCE";
    public static final String PAGE_SERVER_CONTAINER_SUFFIX = "CONTAINER";

}
