package de.tudresden.inf.st.pnml.base.util;

import de.tudresden.inf.st.pnml.jastadd.model.Node;
import de.tudresden.inf.st.pnml.jastadd.model.Place;

public class NavUtils {

    public static Place getOriginPlace(Node p){

        if(p.asRefPlace() != null){
            return p.asPlaceNode().place();
        } else {
            return getOriginPlace(p);
        }
    }
}
