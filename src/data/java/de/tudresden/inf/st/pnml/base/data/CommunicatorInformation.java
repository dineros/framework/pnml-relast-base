package de.tudresden.inf.st.pnml.base.data;

import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;

public class CommunicatorInformation {

   // private HashMap<String, String> communicatorMapping;
    private MultiValuedMap<String, String> communicatorMapping;

    public CommunicatorInformation(){
        communicatorMapping = new ArrayListValuedHashMap<>();
    }

    public void addMapping(String type, String net){
        communicatorMapping.put(type, net);
    }

    public MultiValuedMap<String, String> getCommunicatorMapping() {
        return communicatorMapping;
    }
}
