package de.tudresden.inf.st.pnml.base.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileUtils {

    public static File resolveTemplate(String path) {
        // Get the ClassLoader
        ClassLoader classLoader = FileUtils.class.getClassLoader();

        // Get the URL to the resource
        URL resourceURL = classLoader.getResource(path);

        if (resourceURL != null) {
            try {
                // If the resource is inside the JAR (URL starts with "jar:file:")
                if (resourceURL.getProtocol().equals("jar")) {
                    // If the resource is inside the JAR, copy it to a temporary file
                    InputStream inputStream = resourceURL.openStream();
                    Path tempFile = Files.createTempFile("template_", ".pnml");
                    try (OutputStream outputStream = Files.newOutputStream(tempFile)) {
                        byte[] buffer = new byte[1024];
                        int length;
                        while ((length = inputStream.read(buffer)) > 0) {
                            outputStream.write(buffer, 0, length);
                        }
                    }
                    return tempFile.toFile(); // Return the temporary File

                } else {
                    // If the resource is not inside a JAR, it is a regular file on the filesystem
                    return new File(resourceURL.getFile()); // Return File for IDE usage
                }
            } catch (IOException e) {
                System.out.println("Error reading resource: " + e.getMessage());
                return null;
            }
        } else {
            System.out.println("Resource not found");
            return null;
        }
    }
}
