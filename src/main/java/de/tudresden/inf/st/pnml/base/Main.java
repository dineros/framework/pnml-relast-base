package de.tudresden.inf.st.pnml.base;

public class Main {

    public static void main(String[] args){

        // Nothing to do here
        // List<PetriNet> petriNets = PnmlParser.parsePnml("...");
        // printNet(petriNets.get(0), false, false);

    }

    /*public static void printNet(PetriNet petriNet, boolean withArcs, boolean withToolSpecifics) {

        System.out.println("--------------- STRUCTURE ---------------");
        System.out.println("----------------- PLACES ----------------");

        for (Place p : petriNet.allPlaces()) {
            System.out.println("Place " + p.asDinerosPlace().getId() + " -- " + p.asDinerosPlace().getStaticPlaceInformation().getSubNet());
            if (p.getInitialMarking() != null) {
                System.out.println("--- Marking: " + p.getInitialMarking().getText());
            } else {
                System.out.println("--- Marking: NULL");
            }
        }

        System.out.println("-------------- TRANSITIONS --------------");

        for (Transition t : petriNet.allTransitions()) {
            System.out.println("Transition " + t.getId());
        }

        System.out.println("-------------- TRANSITION DETAILS --------------");

        for (Transition t : petriNet.allTransitions()) {

            if (t.asDinerosTransition().getStaticTransitionInformation().isServiceTransitionInformation()) {
                System.out.println("--- Transition: " + t.getId() + " subnet: " + t.asDinerosTransition().getStaticTransitionInformation().getSubNet()
                        + " service: " + t.asDinerosTransition().getStaticTransitionInformation().asServiceTransitionInformation().getServiceName() + " ---------");
            } else if (t.asDinerosTransition().getStaticTransitionInformation().isTopicTransitionInformation()) {
                System.out.println("--- Transition: " + t.getId() + " subnet: " + t.asDinerosTransition().getStaticTransitionInformation().getSubNet()
                        + " topic: " + t.asDinerosTransition().getStaticTransitionInformation().asTopicTransitionInformation().getTopic() + " ---------");
            } else {
                System.out.println("--- Transition: " + t.getId() + " subnet: " + t.asDinerosTransition().getStaticTransitionInformation().getSubNet() + " --- name: " + t.getName().getText());
            }

            for (Place p : t.asDinerosTransition().incomingPlaces()) {

                System.out.println("------ Inputplace:  " + p.getId() + " subnet: " + p.asDinerosPlace().getStaticPlaceInformation().getSubNet() + " ---------");
            }

            for (Place p : t.asDinerosTransition().outgoingPlaces()) {

                System.out.println("------ Outputplace: " + p.getId() + " subnet: " + p.asDinerosPlace().getStaticPlaceInformation().getSubNet() + " ---------");
            }
        }

        System.out.println("----------------- REF PLACES -----------------");

        for (RefPlace rp : petriNet.allRefPlaces()) {
            System.out.println("--- RefPlace: " + rp.getId());
        }

        System.out.println("----------------- REF TRANSITIONS -----------------");

        for (RefTransition rt : petriNet.allRefTransitions()) {
            System.out.println("--- RefTransition: " + rt.getId());
        }

        if (withArcs) {
            System.out.println("----------------- ARCS -----------------");

            for (Arc a : petriNet.allArcs()) {
                System.out.println("Arc: " + a.getId() + " -- source: " + a.getSource().getId() + " -- target: " + a.getTarget().getId());
            }
        }


        System.out.println("--------------- T SIGNALS (STATIC)---------------");

        for (Transition t : petriNet.allTransitions()) {
            DinerosTransition ist = t.asDinerosTransition();

            if (ist != null && ist.getMutableTransitionInformation() == null) {
                if(ist.getStaticTransitionInformation().isDefaultTransitionInformation()){
                    System.out.println(ist.getStaticTransitionInformation().asDefaultTransitionInformation().getInputSignalClause().printClause());
                }
            }
        }

        if(withToolSpecifics) {
            System.out.println("--------------- TOOL SPECIFIC ---------------");

            for (Transition t : petriNet.allTransitions()) {
                DinerosTransition ist = t.asDinerosTransition();
                if (ist != null && ist.getNumToolspecific() > 0) {
                    System.out.println("ToolSpecific: (" + ist.getName().getText() + ") " + ist.getToolspecific(0).getFormattedXMLBuffer().toString());
                }
            }
        }
    }*/
}
