package de.tudresden.inf.st.pnml.jastadd.scanner;

import de.tudresden.inf.st.pnml.jastadd.parser.ExpressionParser.Terminals;

%%

%public
%final
%class ExpressionScanner
%extends beaver.Scanner

%type beaver.Symbol
%function nextToken
%yylexthrow beaver.Scanner.Exception

%line
%column

%{
  private beaver.Symbol sym(short id) {
    return new beaver.Symbol(id, yyline + 1, yycolumn + 1, yylength(), yytext());
  }
%}

WhiteSpace = [ ] | \t | \f
NewLine    = \n | \r | \r\n
Variable = [:jletter:][:jletterdigit:]*

%%

// discard whitespace information
{WhiteSpace}  { }

// token definitions
"AND"         { return sym(Terminals.AND); }
"OR"          { return sym(Terminals.OR); }
"NOT"         { return sym(Terminals.NOT); }
"("           { return sym(Terminals.LP); }
")"           { return sym(Terminals.RP); }
{Variable}    { return sym(Terminals.IDENTIFIER); }
<<EOF>>       { return sym(Terminals.EOF); }

/* error fallback */
[^]            { throw new Error("Illegal character '"+ yytext() +"' at line " + (yyline+1) + " column " + (yycolumn+1)); }