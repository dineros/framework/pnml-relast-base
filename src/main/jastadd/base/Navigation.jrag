aspect Navigation {
  inh PetriNet PnObject.petriNet();
  eq PetriNet.getChild().petriNet() = this;

  syn Place RefPlace.originPlace(){
      return de.tudresden.inf.st.pnml.base.util.NavUtils.getOriginPlace(this);
  }

  syn java.util.Collection<Place> TransitionNode.incomingPlaces() {

      java.util.Set<Place> s = new java.util.HashSet<>();

      for (Arc incomingArc : getInArcList()) {
        if(!incomingArc.getSource().asPlaceNode().isRefPlace()){
            s.add(incomingArc.getSource().asPlaceNode().place());
        }
      }

      for (TransitionNode ref : getReferencingTransitions()) {
          s.addAll(ref.incomingPlaces());
      }

      return s;
  }

  syn java.util.Collection<RefPlace> TransitionNode.incomingRefPlaces() {

      java.util.Set<RefPlace> s = new java.util.HashSet<>();

        for (Arc incomingArc : getInArcList()) {
            if(incomingArc.getSource().asPlaceNode().isRefPlace()){
                s.add(incomingArc.getSource().asPlaceNode().asRefPlace());
            }
        }

        for (TransitionNode ref : getReferencingTransitions()) {
            s.addAll(ref.incomingRefPlaces());
        }

        return s;
  }
  
  syn java.util.Collection<Place> TransitionNode.outgoingPlaces() {

    java.util.Set<Place> s = new java.util.HashSet<>();

    for (Arc outgoing : getOutArcList()) {
        if(outgoing.getTarget() != null && outgoing.getTarget().asPlaceNode() != null){
            s.add(outgoing.getTarget().asPlaceNode().place());
        }
    }

    for (TransitionNode ref : getReferencingTransitions()) {
         s.addAll(ref.outgoingPlaces());
    }

    return s;
  }

  inh Page PnObject.ContainingPage();
  eq Page.getObject().ContainingPage() = this;
  eq PetriNet.getPage().ContainingPage() = null;

  inh Set<Page> PnObject.ContainingPageTree();
  eq Page.getObject().ContainingPageTree() {
        Set<Page> res = this.ContainingPageTree();
        res.add(this);
        return res;
  }
  eq PetriNet.getPage().ContainingPageTree() {
        Set<Page> res = new HashSet<Page>();
        res.add(this.getPage(0));
        return res;
  }

  syn boolean PnObject.isPageNode() = false;
  eq Page.isPageNode() = true;

  syn boolean PnObject.isArcNode() = false;
  eq Arc.isArcNode() = true;

  syn boolean PnObject.isPlaceObject() = false;
  eq Place.isPlaceObject() = true;

  syn boolean PnObject.isTransitionObject() = false;
  eq Transition.isTransitionObject() = true;

  syn boolean Node.isPlaceNode() = false;
  eq PlaceNode.isPlaceNode() = true;

  syn PlaceNode Node.asPlaceNode() = null;
  eq PlaceNode.asPlaceNode() = this;

  syn boolean Node.isTransitionNode() = false;
  eq TransitionNode.isTransitionNode() = true;

  syn boolean TransitionInformation.isTopicTransitionInformation() = false;
  eq TopicTransitionInformation.isTopicTransitionInformation() = true;

  syn boolean TransitionInformation.isServiceTransitionInformation() = false;
  eq ServiceTransitionInformation.isServiceTransitionInformation() = true;

  syn boolean TransitionInformation.isSignalTransitionInformation() = false;
  eq SignalTransitionInformation.isSignalTransitionInformation() = true;

  syn boolean TransitionNode.isRefTransition() = false;
  eq RefTransition.isRefTransition() = true;

  syn boolean TransitionNode.isTransition() = false;
  eq Transition.isTransition() = true;

  syn boolean PlaceNode.isRefPlace() = false;
  eq RefPlace.isRefPlace() = true;

  syn boolean PnObject.isRefPlaceObject() = false;
  eq RefPlace.isRefPlaceObject() = true;

  syn RefPlace PnObject.asRefPlaceObject() = null;
  eq RefPlace.asRefPlaceObject() = this;

  syn boolean PnObject.isRefTransitionObject() = false;
  eq RefTransition.isRefTransitionObject() = true;

  syn RefTransition PnObject.asRefTransitionObject() = null;
  eq RefTransition.asRefTransitionObject() = this;

  syn Place PlaceNode.asPlace() = null;
  eq Place.asPlace() = this;

  syn RefPlace Node.asRefPlace() = null;
  eq RefPlace.asRefPlace() = this;

  syn boolean PlaceNode.isPlace() = false;
  eq Place.isPlace() = true;

  syn PublisherPort Port.asPublisherPort() = null;
  eq PublisherPort.asPublisherPort() = this;

  syn SubscriberPort Port.asSubscriberPort() = null;
  eq SubscriberPort.asSubscriberPort() = this;

  syn ServiceChannel Port.asServiceChannel() = null;
  eq ServiceChannel.asServiceChannel() = this;

  syn RefTransition TransitionNode.asRefTransition() = null;
  eq RefTransition.asRefTransition() = this;

  syn Transition TransitionNode.asTransition() = null;
  eq Transition.asTransition() = this;

  syn Node PnObject.asNode() = null;
  eq Node.asNode() = this;

  syn TransitionNode Node.asTransitionNode() = null;
  eq TransitionNode.asTransitionNode() = this;

  syn DinerosTransition PnObject.asDinerosTransition() = null;
  eq DinerosTransition.asDinerosTransition() = this;

  syn Page PnObject.asPage() = null;
  eq Page.asPage() = this;

  syn Arc PnObject.asArc() = null;
  eq Arc.asArc() = this;

  syn DinerosPlace PnObject.asDinerosPlace() = null;
  eq DinerosPlace.asDinerosPlace() = this;

  syn TopicTransitionInformation TransitionInformation.asTopicTransitionInformation() = null;
  eq TopicTransitionInformation.asTopicTransitionInformation() = this;

  syn ServiceTransitionInformation TransitionInformation.asServiceTransitionInformation() = null;
  eq ServiceTransitionInformation.asServiceTransitionInformation() = this;

  syn SignalTransitionInformation TransitionInformation.asSignalTransitionInformation() = null;
  eq SignalTransitionInformation.asSignalTransitionInformation() = this;

  syn Place PlaceNode.place();
  eq Place.place() = this;
  eq RefPlace.place() = getRef().place();

  syn Transition TransitionNode.transition();
  eq Transition.transition() = this;
  eq RefTransition.transition() = getRef().transition();

  coll java.util.Set<PnObject> PetriNet.allObjects() [new java.util.HashSet()] root PetriNet;
  PnObject contributes this
    to PetriNet.allObjects()
    for petriNet();

  coll java.util.Set<Place> PetriNet.allPlaces() [new java.util.HashSet()] root PetriNet;
  Place contributes this
    to PetriNet.allPlaces()
    for petriNet();

  coll java.util.Set<PlaceNode> PetriNet.allPlaceNodes() [new java.util.HashSet()] root PetriNet;
  PlaceNode contributes this
    to PetriNet.allPlaceNodes()
    for petriNet();

  coll java.util.Set<Transition> PetriNet.allTransitions() [new java.util.HashSet()] root PetriNet;
  Transition contributes this
    to PetriNet.allTransitions()
    for petriNet();

  coll java.util.Set<DinerosTransition> PetriNet.allDinerosTransitions() [new java.util.HashSet()] root PetriNet;
  DinerosTransition contributes this
    to PetriNet.allDinerosTransitions()
    for petriNet();

  coll java.util.Set<DinerosPlace> PetriNet.allDinerosPlaces() [new java.util.HashSet()] root PetriNet;
  DinerosPlace contributes this
     to PetriNet.allDinerosPlaces()
     for petriNet();

  coll java.util.Set<Arc> PetriNet.allArcs() [new java.util.HashSet()] root PetriNet;
  Arc contributes this
    to PetriNet.allArcs()
    for petriNet();

  coll java.util.Set<Page> PetriNet.allPages() [new java.util.HashSet()] root PetriNet;
  Page contributes this
    to PetriNet.allPages()
    for petriNet();

  coll java.util.Set<TransitionNode> PetriNet.allTransitionNodes() [new java.util.HashSet()] root PetriNet;
  TransitionNode contributes this
    to PetriNet.allTransitionNodes()
    for petriNet();

  coll java.util.Set<RefTransition> PetriNet.allRefTransitions() [new java.util.HashSet()] root PetriNet;
  RefTransition contributes this
    to PetriNet.allRefTransitions()
    for petriNet();

  coll java.util.Set<RefPlace> PetriNet.allRefPlaces() [new java.util.HashSet()] root PetriNet;
  RefPlace contributes this
    to PetriNet.allRefPlaces()
    for petriNet();

   syn Transition PetriNet.getTransitionFromTransitionNode(TransitionNode tn) {

      for (Transition t : this.allTransitions()) {
         if (Objects.equals(t.getId(), tn.getId())) {
             return t;
         }
      }
      return null;
   }

   syn Place PetriNet.getPlaceFromPlaceNode(PlaceNode pNode) {

       for (Place p : this.allPlaces()) {
          if (Objects.equals(p.getId(), pNode.getId())) {
             return p;
          }
       }
       return null;
   }

    syn RefPlace PetriNet.getRefPlaceFromPlaceNode(PlaceNode pNode) {

        if (pNode.isRefPlace()) {
            for (RefPlace p : this.allRefPlaces()) {
                if (Objects.equals(p.getId(), pNode.getId())) {
                    return p;
                }
            }
        }
        return null;
   }

   syn Transition PetriNet.getTransitionById(String id) {
        for (Transition t : this.allTransitions()) {
            if (t.getId().equals(id)) {
                return t;
            }
        }
        return null;
   }

   syn Place PetriNet.getPlaceById(String id) {
          for (Place p : this.allPlaces()) {
              if (p.getId().equals(id)) {
                return p;
              }
          }
          return null;
   }

   syn String PetriNet.PlaceStringExport() {

          String res = "";

          for (Place p : this.allPlaces()) {
              res += p.getId() + ", ";
          }
          return res;
   }

   syn RefPlace PetriNet.getRefPlaceById(String id) {
          for (RefPlace rp : this.allRefPlaces()) {
              if (rp.getId().equals(id)) {
                return rp;
              }
          }
          return null;
   }
   
   syn List<Place> PetriNet.getPlacesWithContainingString(String s) {

          List<Place> res = new ArrayList<>();

          for (Place p : this.allPlaces()) {
              if (p.getId().contains(s)) {
                res.add(p);
              }
          }
          return res;
   }

    syn RefTransition PetriNet.getRefTransitionFromTransitionNode(TransitionNode tNode) {

        if (tNode.isRefTransition()) {
            for (RefTransition t : this.allRefTransitions()) {
                if (Objects.equals(t.getId(), tNode.getId())) {
                    return t;
                }
            }
        }
        return null;
    }
}
